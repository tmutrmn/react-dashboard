import openSocket from 'socket.io-client';
const io = openSocket('http://localhost:3333/');


const socket = (callback) => {
	// listen for any incoming messages
	/*
  socket.on('chat', (message) => {
    console.log(message)
    cb(message);
	})
	*/

	io.on('chat', (msg) => {
		callback(msg);
	});

}

export { socket }