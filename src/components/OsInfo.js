import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class OsInfo extends React.PureComponent {

	render() {
		return (
			<div style={blockDivStyle}>
				<h2>OPERATING SYSTEM</h2>
				<div style={outerContainerDivStyle}>
					<table style={{ width: '100%', padding:'0.5rem'}}>
						<tbody>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Platform:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.os_info.platform }</td>
							</tr>
							<tr>
								<td colSpan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top' }}>Type:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top' }}>{ this.props.os_info.type } </td>
							</tr>
							<tr>
								<td colSpan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top' }}>Architecture:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top' }}>{ this.props.os_info.is64bit ? "64bit": "32bit" } </td>
							</tr>
							<tr>
								<td colSpan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top' }}>Version:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top' }}>{ this.props.os_info.release } </td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}