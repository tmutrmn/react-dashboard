import React from 'react';
import ContainerDimensions from 'react-container-dimensions'
import Map from 'pigeon-maps'
import Marker from 'pigeon-marker'
import Overlay from 'pigeon-overlay'

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class MapInfo extends React.PureComponent {
	state = {
		zoom: 8
	}

	componentDidUpdate(prevProps) {
		if (this.props.lat !== prevProps.lat || this.props.lng !== prevProps.lng) {
			// this.fetchWeatherData(this.props.coords.latitude, this.props.coords.longitude);
		}
	}

	
  render() {
    return (
			<div style={blockDivStyle}>
				<h2>MAP</h2>
				<div style={outerContainerDivStyle}>
					<ContainerDimensions>
						{ ({ width, height }) => 
							<div style={innerContainerDivStyle}>
								<button style={{ position:'absolute', width:'24px', height:'24px', left:'0', bottom:'0', zIndex:'9999' }} onClick={ () => this.setState({ zoom: this.state.zoom + 1 }) }>+</button>
								<button style={{ position:'absolute', width:'24px', height:'24px', left:'24px', bottom:'0', zIndex:'9999' }} onClick={ () => this.setState({ zoom: this.state.zoom - 1 }) }>-</button>
								<Map center={[this.props.lat, this.props.lng]} zoom={this.state.zoom} width={width} height={height}>
									<Marker anchor={[this.props.lat, this.props.lng]} payload={1} onClick={({ event, anchor, payload }) => { }} />
								</Map>
							</div>
						}
          </ContainerDimensions>
				</div>
			</div>
    )
  }

}