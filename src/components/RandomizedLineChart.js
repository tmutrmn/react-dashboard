import React from 'react';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, } from 'recharts';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


const getRandomInt = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


const getState = () => ({
	data: [
		{ name: 'A', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
		{ name: 'B', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
    { name: 'C', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
    { name: 'D', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
    { name: 'E', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
    { name: 'F', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
    { name: 'G', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
	]
});



export default class RandomizedLineChart extends React.PureComponent {

	constructor(props) {
		super(props);
		console.log("RandomizedLineChart: ", this.props)
		this.state = {
			data: [
				{ name: 'A', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'B', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'C', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'D', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'E', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'F', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
        { name: 'G', uv: getRandomInt(5, 25), pv: getRandomInt(5, 25), amt: getRandomInt(50, 100) },
			]
		}
	}

	componentDidMount() {
		setInterval(() => {
			this.setState(getState());
		}, 3000);
	}


	render() {
		if (!this.props.layout || this.props.layout === 'horizontal') {
			return (
				<div style={blockDivStyle}>
					<h2>HORIZONTAL RANDOMIZED LINE CHART</h2>
					<div style={outerContainerDivStyle}>
						<div style={innerContainerDivStyle}>
							<ResponsiveContainer width="100%" height="100%">
								<LineChart data={this.state.data}
									layout='horizontal'
									margin={{top: 5, right: 30, left: 0, bottom: 5}}>
									<XAxis dataKey="name"/>
									<YAxis/>
									<CartesianGrid strokeDasharray="3 3"/>
									<Legend />
									<Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{r: 8}}/>
									<Line type="monotone" dataKey="uv" stroke="#82ca9d" />
								</LineChart>
							</ResponsiveContainer>
						</div>
					</div>
				</div>
			)
		} else {
			return (
				<div style={blockDivStyle}>
					<h2>VERTICAL RANDOMIZED LINE CHART</h2>
					<div style={outerContainerDivStyle}>
						<div style={innerContainerDivStyle}>
							<ResponsiveContainer width="100%" height="100%">
								<LineChart data={this.state.data}
									layout="vertical"
            			margin={{top: 20, right: 30, left: 20, bottom: 5}}>
									<CartesianGrid strokeDasharray="3 3"/>
									<XAxis type="number" />
									<YAxis dataKey="name" type="category"/>
									<Tooltip/>
									<Legend />
									<Line dataKey="pv" stroke="#8884d8" />
									<Line dataKey="uv" stroke="#82ca9d" />
								</LineChart>
							</ResponsiveContainer>
						</div>
					</div>
				</div>
			)
		}
	}

}