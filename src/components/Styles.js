export const blockDivStyle = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'stretch',
	justifyContent: 'space-around',
	minHeight: '100%',
	height: 'auto',
};

export const outerContainerDivStyle = {
	display: 'flex',
	flex: 1,
	alignSelf: 'stretch',
	position: 'relative',
	width: '100%',
	minHeight: '100%',
	height: 'auto',
};

export const innerContainerDivStyle = {
	position: 'absolute',
	top: 0,
	right: 0,
	bottom: 0,
	left: 0,
	width: '100%',
	height: '100%',
};