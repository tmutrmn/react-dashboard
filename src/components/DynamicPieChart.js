import React, { PureComponent } from 'react';
import { ResponsiveContainer, PieChart, Pie, Sector, Cell, } from 'recharts';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;

const renderCustomizedLabel = ({
	cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}) => {
	const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
	const x = cx + radius * Math.cos(-midAngle * RADIAN);
	const y = cy + radius * Math.sin(-midAngle * RADIAN);

	return (
		<text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
			{`${(percent * 100).toFixed(0)}%`}
		</text>
	);
};


const getRandomInt = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


const getState = () => ({
	data: [
		{ name: 'A', value: getRandomInt(5, 33) },
		{ name: 'B', value: getRandomInt(5, 33) },
		{ name: 'C', value: getRandomInt(5, 33) },
	]
});



export default class DynamicPieChart extends React.PureComponent {

	constructor(props) {
		super(props);
		this.state = {
			data: [
				{ name: 'A', value: getRandomInt(5, 33) },
				{ name: 'B', value: getRandomInt(5, 33) },
				{ name: 'C', value: getRandomInt(5, 33) },
			]
		}
	}

	componentDidMount() {
		setInterval(() => {
			this.setState(getState());
		}, 4000);
	}


	render() {
		// label={renderCustomizedLabel}
		return (
			<div style={blockDivStyle}>
				<h2>DYNAMIC PIE CHART</h2>
				<div style={outerContainerDivStyle}>
					<div style={innerContainerDivStyle}>
						<ResponsiveContainer width="100%" height="100%">
							<PieChart>
								<Pie
									data={this.state.data}
									innerRadius={'60%'}
									outerRadius={'80%'}
									paddingAngle={5}
									labelLine={false}
									fill="#8884d8"
									dataKey="value"
								>
									{
										this.state.data.map((entry, index) => <Cell isAnimationActive={false} key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
									}
								</Pie>
							</PieChart>
						</ResponsiveContainer>
					</div>
				</div>
			</div>
		);
	}

}