import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


const outputTotalAmount = (amount) => Math.ceil(Number(amount) / (1024 * 1024 * 1024)).toFixed(2) + "GB"

const outputFreeAmount = (amount) => Number((amount) / (1024 * 1024 * 1024)).toFixed(2) + "GB"


export default class MemoryInfo extends React.PureComponent {

	render() {
		return (
			<div style={blockDivStyle}>
				<h2>MEMORY</h2>
				<div style={outerContainerDivStyle}>
					<table style={{ width: '100%', padding:'0.5rem'}}>
						<tbody>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Total:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ outputTotalAmount(this.props.total) }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>In use:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ outputFreeAmount(this.props.free) }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Available:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ outputFreeAmount(this.props.total - this.props.free) }</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}