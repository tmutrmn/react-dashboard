import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class DashboardInfo extends React.PureComponent {

	render() {
		return (
			<div>
				<h2>REACT DASHBOARD</h2>
				
					<p style={{margin:'1rem'}}>
						React Dashboard example with dynamic grid layout and realtime data
					</p>
				
			</div>
		);
	}

}