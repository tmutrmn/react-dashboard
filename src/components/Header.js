import React from 'react';
//import { NavLink } from 'react-router-dom';

//import logo from '../logo.svg';
import '../App.css';


const Header = () => (
	<header className="App-header">
		<h1>Real Time Socket Dashboard</h1>
	</header>
)

/*
<div className="ui five item menu">
	<NavLink className="item" activeClassName="active" exact to="/">Home</NavLink> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 
	<NavLink className="item" activeClassName="active" exact to="/page1">Page1</NavLink> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; 
	<NavLink className="item" activeClassName="active" exact to="/page2">Page2</NavLink> 
</div>
*/

export default Header;

