import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class WeatherInfo extends React.PureComponent {

	componentDidUpdate(prevProps) {
		if (this.props.data !== prevProps.data) {
			// this.fetchWeatherData(this.props.coords.latitude, this.props.coords.longitude);
		}
	}


	render() {
		console.log("props: ", this.props)
		if (this.props.data && this.props.data != null)
			return (
				<div style={blockDivStyle}>
					<h2>WEATHER</h2>
					<div style={outerContainerDivStyle}>
						<table style={{ width: '100%', padding:'0.5rem'}}>
							<tbody>
								<tr>
									<td style={{ textAlign: 'left', verticalAlign: 'top'}}>{ this.props.data.name }</td>
									<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.main.temp >= 0 ? "+ " + this.props.data.main.temp.toFixed(1) + "°C" : this.props.data.main.temp.toFixed(1) + "°C" }</td>
								</tr>
								<tr>
									<td style={{ textAlign: 'left', verticalAlign: 'top'}}>{ this.props.data.weather[0].main }</td>
									<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.weather[0].description }</td>
								</tr>
								<tr>
									<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Humidity</td>
									<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.main.humidity }</td>
								</tr>
								<tr>
									<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Wind</td>
									<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.wind.speed } m/s</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			)
		else return null
	}

}