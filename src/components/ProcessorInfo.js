import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class ProcessorInfo extends React.PureComponent {

	render() {
		return (
			<div style={blockDivStyle}>
				<h2>PROCESSOR</h2>
				<div style={outerContainerDivStyle}>
					<table style={{ width: '100%', padding:'0.5rem'}}>
						<tbody>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Model:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.model }</td>
							</tr>
							<tr>
								<td colSpan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top' }}>Cores:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top' }}>{ this.props.cores }</td>
							</tr>
							<tr>
								<td colSpan='2'>&nbsp;</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top' }}>Speed:</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top' }}>{ this.props.speed } MHz</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}