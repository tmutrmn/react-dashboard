import React from 'react';
import { RadialBarChart, RadialBar, Legend } from 'recharts';
import { ResponsiveContainer, PieChart, Pie } from 'recharts';
import SocketContext from '../context/SocketContext'

import * as randomColor from 'randomcolor'

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


const COLORS = ['LIME', 'AQUA', 'YELLOW', 'FUCHSIA'];
const COLORS2 = ['#8884d8', '#83a6ed', '#8dd1e1', '#82ca9d', '#a4de6c', '#d0ed57', '#ffc658']

const style = {
	top: 0,
	left: 350,
	lineHeight: '24px'
};


class CoreUsage extends React.Component {

    constructor(props) {
			super(props);
			console.log("CoreUsage: ", this.props)
			this.state = {
				cpu_usage: [],		// { value: 100, fill: 'transparent' }, { value: 0, fill: '#8884d8' }],
				percentage: 0,
				colors: [],
				ready: false,
			}
    }


    componentDidMount() {
			this.props.socket.on("CPU_USAGE", data => {
				if (!this.state.ready) {
					const colors = randomColor({
						seed: 1234,
						count: data.cpu_usage.length,
						luminosity: 'light',
						hue: 'purple',
					})

					const filtered = []
					filtered.push({ value: 100, fill: 'transparent' })		// MUST HAVE MAX VALUE!!!
					for (let i = 0; i < data.cpu_usage.length; i++) {
						filtered.push({ value: 0, fill: '#ffffff' })
					}
					this.setState({ colors, cpu_usage: filtered, ready: true })
				}

				if (this.props.radial) {
					const filtered = []
					filtered.push({ value: 100, fill: 'transparent' })
					for (let i = 0; i < data.cpu_usage.length; i++) {		// data.cpu_usage[i].value
						filtered.push({ value: data.cpu_usage[i].value, fill: this.state.colors[i] })
					}
					this.setState({ cpu_usage: filtered })
				} else if (!this.props.radial && data.cpu_usage[this.props.core] != null) {
					const filtered = []
					filtered.push({ value: 100, fill: 'transparent' })		// MUST HAVE MAX VALUE!!!
					filtered.push(data.cpu_usage[this.props.core])
					this.setState({ cpu_usage: filtered, percentage: data.cpu_usage[this.props.core].value })
				}
			})
    }


    render() {
			if (this.props.radial) {
				return (
					<div style={blockDivStyle}>
						<h2>All Cores</h2>
						<div style={outerContainerDivStyle}>
							<div style={innerContainerDivStyle}>
							<ResponsiveContainer width="100%" height="100%">
								<RadialBarChart
									data={this.state.cpu_usage}
									startAngle={180} 
  								endAngle={0}
									innerRadius={20}
									outerRadius={140}
									barSize={10}
									cx={"50%"} cy={"75%"}>
									<RadialBar minAngle={1} background clockWise={false} dataKey='value'/>
								</RadialBarChart>
							</ResponsiveContainer>
							</div>
						</div>
					</div>
				)
			} else {
				return (
					<div style={blockDivStyle}>
						<h2>Core { this.props.core || null }</h2>
						<div style={outerContainerDivStyle}>
							<div style={innerContainerDivStyle}>
								<ResponsiveContainer width="100%" height="100%">
									<PieChart>
										<Pie dataKey="value" data={this.state.cpu_usage} fill={COLORS[this.state.percentage / 25 - 1]} innerRadius="60%" outerRadius="80%" />
									</PieChart>
								</ResponsiveContainer>
							</div>
						</div>
					</div>
				)
			}
    }

}


const CoreUsageWithSocket = props => (
  <SocketContext.Consumer>
		{ socket => <CoreUsage {...props} socket={socket} /> }
  </SocketContext.Consumer>
)

export default CoreUsageWithSocket