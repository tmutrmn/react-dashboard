import React from 'react'
import ContainerDimensions from 'react-container-dimensions'
import Clock from 'react-clock'

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class AnalogClock extends React.PureComponent {

	constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    }
  }
  
  componentDidMount() {
    setInterval(
      () => this.setState({ date: new Date() }),
      1000
    );
  }

	render() {
		return (
      <div style={blockDivStyle}>
        <h2>TIME</h2>
        <div style={outerContainerDivStyle}>
          <ContainerDimensions>
            { 
              ({ width, height }) => 
                height < width ? <Clock value={this.state.date} size={height-20} /> : <Clock value={this.state.date} size={width-20} />
            }
          </ContainerDimensions>
        </div>
      </div>
    )
  }

}
