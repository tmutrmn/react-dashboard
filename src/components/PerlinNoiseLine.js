import React from 'react';
import SocketContext from '../context/SocketContext'
// import socketIOClient from "socket.io-client";

import { ResponsiveContainer, LineChart, Line } from 'recharts';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


class PerlinNoiseLine extends React.Component {

	constructor() {
		super();
		
    this.state = {
      response: false,
			endpoint: "http://127.0.0.1:3333",
			perlin_xOffset: 0,
			interval: null,
			data: []
    };
  }

	
	componentDidMount() {
		const data = []
		for (let i = 0; i < 16; i++ ) {
			data.push({ x: i, y: 0})
		}
		this.setState({ data: data });

		this.props.socket.emit('REQUEST_HELLO_WORLD')
		this.props.socket.on('HELLO_WORLD', (msg) => {
			console.log(msg);
		})
		
		this.props.socket.on("PERLIN_NOISE", data => {
			var newState = {
				data: data.noise,
				perlin_xOffset: this.state.perlin_xOffset + 1,
			};

			this.setState(newState);
		});

		/*
		interval = setInterval(() => {
			this.props.socket.emit('REQUEST_PERLIN_NOISE', { width: 16, height: 16, xOffset: this.state.perlin_xOffset })
		}, 1000)
		*/
	}
	

	componentWillUnmount() {
		/*
		if (interval) {
			clearInterval(interval)
		}
		*/
	}


	getMaxY = () => {
		const dataMax = Math.max(...this.state.data.map((i) => i.y))
		const dataMin = Math.min(...this.state.data.map((i) => i.y))

		if (dataMin >= dataMax) {
			return dataMin
		} else {
			return dataMax
		}
	}

	getMinY = () => {
		const dataMax = Math.max(...this.state.data.map((i) => i.y))
		const dataMin = Math.min(...this.state.data.map((i) => i.y))

		if (dataMax <= dataMin) {
			return dataMax
		} else {
			return dataMin
		}
	}
		
	
	gradientOffset = () => {
		const dataMax = Math.max(...this.state.data.map((i) => i.y))
		const dataMin = Math.min(...this.state.data.map((i) => i.y))
	
		if (dataMax <= 0) {
			return 0
		} else if (dataMin >= 0) {
			return 1
		}
		/*
		else{
			return dataMax / (dataMax - dataMin);
		}
		*/
	}
	
	// const off = gradientOffset();


	render() {
		/*
		<ResponsiveContainer width="100%" height="100%">
			<LineChart data={this.state.data}>
				<Line type='monotone' dataKey='y' stroke='#555' strokeWidth={1} />
			</LineChart>
		</ResponsiveContainer>
		*/	
		/*
		<AreaChart
								data={this.state.data}
								margin={{top: 10, right: 30, left: 0, bottom: 0}}
							>
								<CartesianGrid strokeDasharray="3 3"/>
								<XAxis dataKey="x"/>
								<YAxis/>
								<Tooltip/>
								<defs>
									<linearGradient id="splitColor" x1="0" y1="0" x2="0" y2="10">
										<stop offset={this.gradientOffset()} stopColor="green" stopOpacity={1}/>
										<stop offset={this.gradientOffset()} stopColor="red" stopOpacity={1}/>
									</linearGradient>
								</defs>
								<Area type="monotone" dataKey="y" stroke="#000" fill="url(#splitColor)" />
							</AreaChart>
		*/
		return (
			<div style={blockDivStyle}>
				<h2>2D PERLIN NOISE LINE</h2>
				<div style={outerContainerDivStyle}>
					<div style={innerContainerDivStyle}>
						<ResponsiveContainer width="100%" height="100%">
							<AreaChart data={this.state.data}
								margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
								<defs>
									<linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
										<stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
										<stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
									</linearGradient>
								</defs>
								<XAxis dataKey="x" />
								<YAxis />
								<CartesianGrid strokeDasharray="3 3" />
								<Tooltip />
								<Area type="monotone" dataKey="y" stroke="#82ca9d" fillOpacity={1} fill="url(#colorUv)" />
							</AreaChart>	
						</ResponsiveContainer>
					</div>
				</div>
			</div>
		)
  }

}


const PerlinNoiseLineWithSocket = props => (
  <SocketContext.Consumer>
		{ socket => <PerlinNoiseLine {...props} socket={socket} /> }
  </SocketContext.Consumer>
)

export default PerlinNoiseLineWithSocket