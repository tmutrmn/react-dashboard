import React from 'react';

import { blockDivStyle, outerContainerDivStyle, innerContainerDivStyle } from './Styles'


export default class ExhangeRates extends React.PureComponent {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div style={blockDivStyle}>
				<h2>EXHANGE RATES <span style={{fontSize:'1vmax', paddingLeft:'1rem'}}>{ this.props.data.date }</span></h2>
				<div style={outerContainerDivStyle}>
					<table style={{ width: '100%', padding:'0.5rem'}}>
						<tbody>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>Currency</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>Rate</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / USD</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["USD"] }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / GBP</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["GBP"] }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / HKD</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["HKD"] }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / JPY</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["JPY"] }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / NOK</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["NOK"] }</td>
							</tr>
							<tr>
								<td style={{ textAlign: 'left', verticalAlign: 'top'}}>EUR / SEK</td>
								<td style={{ textAlign: 'right', verticalAlign: 'top'}}>{ this.props.data.rates["SEK"] }</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}