import React, { Component } from 'react';
import Layout from './Layout'
import SocketContext from './context/SocketContext'
import * as io from 'socket.io-client'
// import io from 'socket.io-client/dist/socket.io';

const socket = io('http://localhost:3333')


/*
class App extends Component {
  render() {
    return (
      <SocketContext.Provider value={socket}></SocketContext.Provider>
      
    );
  }
}
*/

const App = (props) => (
  <SocketContext.Provider value={socket}>
    <Layout />
  </SocketContext.Provider>
)

export default App;
