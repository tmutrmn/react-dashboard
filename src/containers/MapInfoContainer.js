import React from "react";
import { geolocated } from 'react-geolocated';
import MapInfo from '../components/MapInfo'


class MapInfoContainer extends React.Component {

  constructor(props) {
		super(props)
		this.state = {
			ready: true,
			data: null,
			lat: 0,
			lng: 0,
		}
	}		


	componentDidUpdate(prevProps) {
		if (this.props.coords !== prevProps.coords) {
			// this.fetchWeatherData(this.props.coords.latitude, this.props.coords.longitude);
		}
	}


	render() {
		if (!this.props.isGeolocationAvailable) return <div>Your browser does not support Geolocation</div>
		else if (!this.props.isGeolocationEnabled) return <div>Geolocation is not enabled</div>
		else if (this.props.coords && this.state.ready) {
			return <MapInfo lat={this.props.coords.latitude} lng={this.props.coords.longitude} />
		} else return <div>Getting the location data&hellip; </div>
  }

}
	
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(MapInfoContainer);