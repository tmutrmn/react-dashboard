import React from "react"
import { WidthProvider, Responsive } from "react-grid-layout"
import SocketContext from '../context/SocketContext'

import ExchangeRatesContainer from './ExchangeRatesContainer'
import WeatherInfoContainer from './WeatherInfoContainer'
import MapInfoContainer from './MapInfoContainer'

import AnalogClock from '../components/AnalogClock'
import DashboardInfo from '../components/DashboardInfo'
import MemoryInfo from '../components/MemoryInfo'
import ProcessorInfo from '../components/ProcessorInfo'
import OsInfo from '../components/OsInfo'
import DynamicPieChart from '../components/DynamicPieChart'
import CoreUsage from '../components/CoreUsage'
import RandomizedLineChart from '../components/RandomizedLineChart'
import PerlinNoiseLine from '../components/PerlinNoiseLine'


import '../../node_modules/react-grid-layout/css/styles.css'
import '../../node_modules/react-resizable/css/styles.css'

import '../Styles.css'


const ResponsiveReactGridLayout = WidthProvider(Responsive);

const loadLayout = (key) => {
  let layout = {}
  if (global.localStorage) {
    try {
      layout = JSON.parse(global.localStorage.getItem("rbd-grl")) || {}
    } catch (e) {
      // error handeling...
    }
  }
  return layout[key]
}

const saveLayout = (key, value) => {
  if (global.localStorage) {
    global.localStorage.setItem(
      "rbd-grl",
      JSON.stringify({ [key]: value })
    )
  }
}

const originalLayouts = loadLayout("layouts") || {};



class GridLayout extends React.PureComponent {

  constructor(props) {
    super(props)
    this.state = {
			layouts: JSON.parse(JSON.stringify(originalLayouts)),
			cpu_info: [{ model: '', speed: 0, times: null }],
			os_info: {
				is64bit: null,
				platform: null,
				release: null,
				type: null
			},
			mem_info: { total: 0, free: 0 },
			num_cores: 0,
			cpu_speed: 0,
			ready: false,
			cpu_usage: [],		// { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }
			test: [1,2,3],
    }
		this.onLayoutChange = this.onLayoutChange.bind(this)
		this.resetLayout = this.resetLayout.bind(this)
		//console.log(JSON.parse(JSON.stringify(originalLayouts)))
		console.log(JSON.stringify(this.state.layouts, null, 4))
	}
	
	static get defaultProps() {
    return {
      className: "layout",
      cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
      rowHeight: 30
    };
  }


	componentDidMount() {
		this.setupSockets(() => this.setState({ ready: true }))		// TODO: this is not the way to do it, find out actual solution (async/await or promises maybe)
	}


	setupSockets(callback) {
		this.props.socket.on('OS_INFO', (data) => {
			this.setState({ os_info: data.os_info })
		})

		this.props.socket.on('CPU_INFO', (data) => {
			const cpu_usage = []
			data.cpu_info.forEach(elem => cpu_usage.push({ value: 0 }))
			this.setState({ num_cores: data.cpu_info.length, cpu_speed: data.cpu_info[0].speed, cpu_info: data.cpu_info, cpu_usage: cpu_usage }, () => {
				// console.log("state: ", this.state, this.state.cpu_usage.length)
			})
		})

		this.props.socket.on('MEM_INFO', (data) => {
			this.setState({ mem_info: data.mem_info })
		})

		if (callback && typeof callback === typeof(Function)) callback()
	}


  resetLayout() {
    this.setState({ layouts: {} });
	}

	onLayoutChange(layout, layouts) {
    saveLayout("layouts", layouts);
    this.setState({ layouts });
	}
	

	renderCoreUsage(core, i) {
		return (
			<div key={`${i+15}`} data-grid={{ w: 1, h: 4, x: i, y: 40, minW: 1, minH: 4 }}>
				<CoreUsage core={i} />
			</div>
		);
	}


  render() {
		if (this.state.ready) {
			return (
				<div>
					<ResponsiveReactGridLayout
						className="layout"
						cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
						rowHeight={30}
						margin={[2, 2]}
						layouts={this.state.layouts}
						onLayoutChange={(layout, layouts) =>
							this.onLayoutChange(layout, layouts)
						}
					>
						<div key="1" data-grid={{ w: 2, h: 6, x: 0, y: 0, minW: 2, minH: 6, }}>
							<DashboardInfo />
							<br />
							<button onClick={this.resetLayout}>Reset Layout</button>
						</div>
						<div key="2" data-grid={{ w: 2, h: 8, x: 2, y: 0, minW: 2, minH: 6 }}>
							<DynamicPieChart />
						</div>
						<div key="3" data-grid={{ w: 3, h: 8, x: 4, y: 0, minW: 3, minH: 6 }}>
							<PerlinNoiseLine />
						</div>
						<div key="4" data-grid={{ w: 3, h: 10, x: 7, y: 0, minW: 3, minH: 6 }}>
							<RandomizedLineChart />
						</div>

						<div key="5" data-grid={{ w: 3, h: 12, x: 7, y: 18, minW: 2, minH: 10 }}>
							<RandomizedLineChart layout='vertical' />
						</div>

						<div key="6" data-grid={{ w: 2, h: 7, x: 10, y: 15, minW: 2, minH: 5 }}>
							<MemoryInfo total={this.state.mem_info.total} free={this.state.mem_info.free} />
						</div>

						<div key="7" data-grid={{ w: 5, h: 7, x: 7, y: 24, minW: 3, minH: 5 }}>
							<ProcessorInfo model={this.state.cpu_info[0].model} cores={this.state.cpu_info.length} speed={this.state.cpu_info[0].speed} />
						</div>

						<div key="8" data-grid={{ w: 2, h: 7, x: 0, y: 22, minW: 2, minH: 7 }}>
							<OsInfo os_info={this.state.os_info} />
						</div>

						<div key="9" data-grid={{ w: 2, h: 8, x: 10, y: 7, minW: 2, minH: 7 }}>
							<ExchangeRatesContainer />
						</div>
						
						<div key="10" data-grid={{ w: 2, h: 7, x: 10, y: 0, minW: 2, minH: 5 }}>
							<WeatherInfoContainer />
						</div>

						<div key="11" data-grid={{ w: 2, h: 8, x: 0, y: 14, minW: 2, minH: 8 }}>
							<CoreUsage radial />
						</div>

						<div key="12" data-grid={{ w: 2, h: 8, x: 0, y: 6, minW: 2, minH: 8 }}>
							<AnalogClock />
						</div>

						<div key="13" data-grid={{ w: 5, h: 21, x: 2, y: 8, minW: 2, minH: 8 }}>
							<MapInfoContainer />
						</div>

						{ this.state.cpu_usage ? this.state.cpu_usage.map((core, i) => this.renderCoreUsage(core, i)) : null }

					</ResponsiveReactGridLayout>
				</div>
			)
		} else {
			return (
				<div>
					loading...
				</div>
			)
		}
	}

}


const GridLayoutWithSocket = props => (
  <SocketContext.Consumer>
		{ socket => <GridLayout {...props} socket={socket} /> }
  </SocketContext.Consumer>
)

export default GridLayoutWithSocket