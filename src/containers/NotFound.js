import React from 'react';

const NotFound = () => (
	<div>
		<h1>NOT FOUND!</h1>
		<p>
			Page not found.<br />
			Go back to <a className="App-link" href="/">Home page</a>
		</p>
	</div>
)

export default NotFound;