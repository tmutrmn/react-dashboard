import React from "react";
import { geolocated } from 'react-geolocated';
import WeatherInfo from '../components/WeatherInfo'


const API_KEY1 = '6d0086b51ad96233a739bf5a4f720749';
const API_URL1 = 'http://api.openweathermap.org/data/2.5';


class WeatherInfoContainer extends React.Component {

  constructor(props) {
		super(props)
		this.state = {
			ready: true,
			data: null,
			lat: 0,
			lng: 0,
		}
	}		


	componentDidUpdate(prevProps) {
		if (this.props.coords !== prevProps.coords) {
			this.fetchWeatherData(this.props.coords.latitude, this.props.coords.longitude);
		}
	}


	fetchWeatherData(lat, lng) {
		// TODO: use async/await + try/catch instead of .then !
		const url = API_URL1 + '/weather?lat=' + lat + '&lon=' + lng + '&id=524901&units=metric&APPID=' + API_KEY1
		fetch(url)
			.then((resp) => resp.json())
				.then((data) => {
					console.log(data)
					this.setState({ data, ready: true })
				})
	}


	render() {
		if (!this.props.isGeolocationAvailable) return <div>Your browser does not support Geolocation</div>
		else if (!this.props.isGeolocationEnabled) return <div>Geolocation is not enabled</div>
		else if (this.props.coords && this.state.ready) {
			return <WeatherInfo data={this.state.data} />
		} else return <div>Getting the location data&hellip; </div>
  }

}
	
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(WeatherInfoContainer);