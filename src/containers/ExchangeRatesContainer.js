import React from "react";

import ExchangeRates from '../components/ExchangeRates'


export default class ExchangeRatesContainer extends React.PureComponent {

  constructor(props) {
		super(props)
		this.state = {
			ready: false,
			data: null
		}
	}		


	componentDidMount() {
		this.fetchExhangeRates()
	}

	fetchExhangeRates() {
		// TODO: use async/await + try/catch instead of .then !
		fetch('https://api.exchangeratesapi.io/latest')
			.then((resp) => resp.json())
				.then((data) => {
					this.setState({ data, ready: true })
				})
	}


	render() {
		if (this.state.ready && this.state.data) {
			return (
				<ExchangeRates data={this.state.data} />
			)
		} else return null 
	}

}
	