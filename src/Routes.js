import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from './components/Header'

import Home from "./containers/Home"
import Page1 from "./containers/Page1"
import Page2 from "./containers/Page2"
import NotFound from "./containers/NotFound"


const Routes = () => (
  <BrowserRouter>
    <main>
      <Header />
      <Switch>
				<Route exact path="/" component={Home} />
        <Route exact path="/page1" component={Page1} />
        <Route exact path="/page2" component={Page2} />
        <Route component={NotFound} />
      </Switch>
		</main>
  </BrowserRouter>
)

export default Routes
