import React from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import Routes from './Routes'

import './App.css';



const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
});

const Layout = () => (
	<MuiThemeProvider theme={theme}>
		<div className="App">
			<Routes />
		</div>
	</MuiThemeProvider>
)


export default Layout