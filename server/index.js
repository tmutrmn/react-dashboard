const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = 3333;
const os = require('os');
const cpustat = require('cpu-stat');


const isOSWin64 = () => {
  return process.arch === 'x64' || process.env.hasOwnProperty('PROCESSOR_ARCHITEW6432');
}


//let cpuPoll = null;
let cpu_info = os.cpus();
let mem_info = { total: os.totalmem(), free: os.freemem() };
let os_info = { platform: os.platform(), release: os.release(), type: os.type(), is64bit: isOSWin64() };
let numCores = cpustat.totalCores();
let cpuUsage = new Array(numCores);


const randBetween = (maximum, minimum) => {
	return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}



app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});



io.set('origins', '*:*');


let interval;
let perlinNoiseInterval;


/*** perlin noise ***/
let M = 4294967296;
let A = 1664525;		// a - 1 should be divisible by m's prime factors
let C = 1;					// c and m should be co-prime
var Z = Math.floor(Math.random() * M);

function rand(){
  Z = (A * Z + C) % M;
  return Z / M - 0.5;
};

function interpolate(pa, pb, px){
	var ft = px * Math.PI,
		f = (1 - Math.cos(ft)) * 0.5;
	return pa * (1 - f) + pb * f;
}

let w = 500;
let h = 300;
let xOffset = 0;

let x = 0;
let y = h / 2;
let amp = 100;			//amplitude
let wl = 50;				//wavelength
let fq = 1 / wl;		//frequency
let a = rand();
let b = rand();

let noise = [];
/*** perlin noise ***/


io.on('connection', async (socket) => {
	console.log('a user connected');

	socket.on('REQUEST_HELLO_WORLD', (data) => {
		console.log('REQUEST_HELLO_WORLD')
		io.emit('HELLO_WORLD', "hello world");
	})

/*
	if (interval) {
    clearInterval(interval);
  }
	interval = setInterval(() => {
		io.emit('HELLO_WORLD', "hello world");
	}, 5000);
*/


console.log(cpu_info, numCores)
socket.emit('CPU_INFO', { cpu_info: cpu_info });
socket.emit('OS_INFO', { os_info: os_info });
socket.emit('MEM_INFO', { mem_info: mem_info });


if (interval) {
	clearInterval(interval);
}
interval = setInterval(() => {
	for (let i = 0; i < numCores; i++) {
		cpustat.usagePercent({
			coreIndex: i,
			sampleMs: randBetween(1000, 1900) + 10 * i,
		}, function (err, percent, seconds) {
			if (err) {
				return console.log(err);
			}
			// console.log(i + ": " , percent, seconds);		// the percentage cpu usage over all cores , the approximate number of seconds the sample was taken over
			cpuUsage[i] = { value: percent };
			socket.emit('CPU_USAGE', { cpu_usage: cpuUsage });
			i++;
		});
	}
}, 5000);		//



if (perlinNoiseInterval) {
	clearInterval(perlinNoiseInterval);
}
perlinNoiseInterval = setInterval(() => {
	w = 256;
	h = 16;
	xOffset = 0;

	w = w + xOffset;
	x = xOffset;
	while (noise.length > 0) noise.pop();		// empty noise array

	while (x < w) {
		if (x % wl === 0) {
			a = b;
			b = rand();
			y = h / 2 + a * amp + amp
		} else {
			y = h / 2 + interpolate(a, b, (x % wl) / wl) * amp + amp
		}
		noise.push({ x, y });
		x += 1;
	}

	socket.emit('PERLIN_NOISE', { noise: noise });
}, 1000);

/*
	socket.on('REQUEST_PERLIN_NOISE', (data) => {
		//console.log('REQUEST_PERLIN_NOISE')
		w = data.width;
		h = data.height;
		xOffset = data.xOffset;

		w = w + xOffset;
		x = xOffset;
		while (noise.length > 0) noise.pop();		// empty noise array

		while (x < w) {
			if (x % wl === 0) {
				a = b;
				b = rand();
				y = h / 2 + a * amp;
			} else {
				y = h / 2 + interpolate(a, b, (x % wl) / wl) * amp;
			}
			noise.push({ x, y });
			x += 1;
		}

		socket.emit('PERLIN_NOISE', { noise: noise });
	});
*/

	socket.on('error', (err) => {
    console.log('received error from client:', io.id)
    console.log(err)
	})
	

	socket.on('disconnect', () => {
		console.log('connection ended');
	})

});



io.listen(port);
console.log('listening on port ', port);

/*
http.listen(port, () => {
  console.log('listening on *:3030');
});
*/